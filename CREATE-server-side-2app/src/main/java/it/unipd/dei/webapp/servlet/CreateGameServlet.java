package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.CreateGameDatabase;
import it.unipd.dei.webapp.resource.Game;
import it.unipd.dei.webapp.resource.Message;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Creates a new game into the database. 
 * 
 * @author Andrea Tommasi
 * @version 1.00
 * @since 1.00
 */
public final class CreateGameServlet extends AbstractDatabaseServlet {

	/**
	 * Creates a new game into the database. 
	 * 
	 * @param req
	 *            the HTTP request from the client.
	 * @param res
	 *            the HTTP response from the server.
	 * 
	 * @throws ServletException
	 *             if any error occurs while executing the servlet.
	 * @throws IOException
	 *             if any error occurs in the client/server communication.
	 */
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		// request parameters
		int id = -1;
		String category = null;
		String name = null;
		String ageRange = null;
		double price = -1;

		// model
		Game e  = null;
		Message m = null;

		try{
			// retrieves the request parameters
			id = Integer.parseInt(req.getParameter("id"));
			category = req.getParameter("category");
			name = req.getParameter("name");
			ageRange = req.getParameter("ageRange");
			price = Double.parseDouble(req.getParameter("price"));

			// creates a new employee from the request parameters
			e = new Game(id, category, name, ageRange, price);

			// creates a new object for accessing the database and stores the employee
			new CreateGameDatabase(getDataSource().getConnection(), e).createGame();
			
			m = new Message(String.format("Game %s successfully created.", category));

		} catch (NumberFormatException ex) {
			m = new Message("Cannot create the game. Invalid input parameters: id must be integer.", 
					"E100", ex.getMessage());
		} catch (SQLException ex) {
			if (ex.getSQLState().equals("23505")) {
				m = new Message(String.format("Cannot create the game: game %s already exists.", name),
						"E300", ex.getMessage());
			} else {
				m = new Message("Cannot create the game: unexpected error while accessing the database.", 
						"E200", ex.getMessage());
			}
		}
		
		
		
		// set the MIME media type of the response
		res.setContentType("text/html; charset=utf-8");

		// get a stream to write the response
		PrintWriter out = res.getWriter();

		// write the HTML page
		out.printf("<!DOCTYPE html>%n");
		
		out.printf("<html lang=\"en\">%n");
		out.printf("<head>%n");
		out.printf("<meta charset=\"utf-8\">%n");
		out.printf("<title>Create Game</title>%n");
		out.printf("</head>%n");

		out.printf("<body>%n");
		out.printf("<h1>Create Game</h1>%n");
		out.printf("<hr/>%n");

		if(m.isError()) {
			out.printf("<ul>%n");
			out.printf("<li>error code: %s</li>%n", m.getErrorCode());
			out.printf("<li>message: %s</li>%n", m.getMessage());
			out.printf("<li>details: %s</li>%n", m.getErrorDetails());
			out.printf("</ul>%n");
		} else {
			out.printf("<p>%s</p>%n", m.getMessage());
			out.printf("<ul>%n");
			out.printf("<li>id: %s</li>%n", e.getId());
			out.printf("<li>category: %s</li>%n", e.getCategory());
			out.printf("<li>name: %s</li>%n", e.getName());
			out.printf("<li>ageRange: %s</li>%n", e.getAgeRange());
			out.printf("<li>price: %s</li>%n", e.getPrice());
			out.printf("</ul>%n");
		}

		out.printf("</body>%n");
		
		out.printf("</html>%n");

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close();
		
	}

}
