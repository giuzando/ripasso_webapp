(function() {
	
	var httpRequest;

	//GAMES QUERY
    var httpRequest = new XMLHttpRequest();
    if (!httpRequest) {
        alert('Giving up :( Cannot create an XMLHttpRequest instance');
        return false;
    }

		//var url = 'http://dbstud.dei.unipd.it:8080/mytoys-server/search-product-by-category?category='+document.getElementById('categoryValue');
		var url = 'http://dbstud.dei.unipd.it:8080/mytoys-server/show-games';

		httpRequest = new XMLHttpRequest();

		if (!httpRequest) {
			alert('Giving up :( Cannot create an XMLHttpRequest instance');
			return false;
		}

		httpRequest.onreadystatechange = writeResults;
		httpRequest.open('GET', url);
		httpRequest.send();
		
		// stop the form from submitting the normal way and refreshing the page
		event.preventDefault();


	function writeResults() {
	
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
		
			// get the div where to write results
			var div = document.getElementById('results');
			
			// clean it up
			div.className = '';
			while (div.firstChild) {
				div.removeChild(div.firstChild);
			}
			
			// generic DOM elements
			var e;
			var ee;
		
			// if we get a 500 status write the error message parsing it from JSON
			if (httpRequest.status == 500) {
			
				div.className = 'alert alert-danger';
			
				// the JSON error message
				var jsonData = JSON.parse(httpRequest.responseText);
				jsonData = jsonData['message'];
				
				e = document.createElement('ul');
				div.appendChild(e);
				
				ee = document.createElement('li');
				ee.appendChild(document.createTextNode('Message: ' + jsonData['message']));
				e.appendChild(ee);
				
				ee = document.createElement('li');
				ee.appendChild(document.createTextNode('Error code: ' + jsonData['error-code']));
				e.appendChild(ee);
				
				ee = document.createElement('li');
				ee.appendChild(document.createTextNode('Error details: ' + jsonData['error-details']));
				e.appendChild(ee);
			
			// if we get a 200 status write result table parsing it from JSON
			} else if (httpRequest.status == 200) {
			
				// a generic row and column of the table
				var tr;
				var td;
			
				// the JSON list of products
				var jsonData = JSON.parse(httpRequest.responseText);
				jsonData = jsonData['resource-list'];
				
				var product;
				
				e = document.createElement('table');
				e.className = 'table';
				div.appendChild(e);
				
				// Prima riga della tabella --- INTESTAZIONE 
				
				ee = document.createElement('thead');
				ee.className = 'thead-light';
				e.appendChild(ee);
				
				tr = document.createElement('tr');
				ee.appendChild(tr);
				
				td = document.createElement('th');
				td.appendChild(document.createTextNode('ID'));
				tr.appendChild(td);
				
				td = document.createElement('th');
				td.appendChild(document.createTextNode('Category'));
				tr.appendChild(td);
				
				td = document.createElement('th');
				td.appendChild(document.createTextNode('Name'));
				tr.appendChild(td);
				
				td = document.createElement('th');
				td.appendChild(document.createTextNode('Age Range'));
				tr.appendChild(td);
				
				td = document.createElement('th');
				td.appendChild(document.createTextNode('Price'));
				tr.appendChild(td);
				
				
				ee = document.createElement('tbody');
				e.appendChild(ee);
				
				//Inserisce nella tabella tutti i prodotti 

				for (var i = 0; i < jsonData.length; i++) {
				
					product = jsonData[i].product;
				
					tr = document.createElement('tr');
					ee.appendChild(tr);
				
					td = document.createElement('td');
					td.appendChild(document.createTextNode(product['id']));
					tr.appendChild(td);
				
					td = document.createElement('td');
					td.appendChild(document.createTextNode(product['category']));
					tr.appendChild(td);
				
					td = document.createElement('td');
					td.appendChild(document.createTextNode(product['name']));
					tr.appendChild(td);
				
					td = document.createElement('td');
					td.appendChild(document.createTextNode(product['ageRange']));
					tr.appendChild(td);
				
					td = document.createElement('td');
					td.appendChild(document.createTextNode(product['price']));
					tr.appendChild(td);
				
				}

			// otherwise write a generic error message
			} else {
				div.className = 'alert alert-danger';
				
				e = document.createElement('p');
				e.appendChild(document.createTextNode('Unexpected error'));
				div.appendChild(e);
			}
		
		}
	}
})();