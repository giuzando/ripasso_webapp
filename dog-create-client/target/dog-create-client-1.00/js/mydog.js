(function()
{
// Load dog list
    var select = document.getElementById('categoryValue'); //variabile cercata, id del form su html
    var httpRequest;
    httpRequest = new XMLHttpRequest();

    if (!httpRequest)
    {
        alert('Giving up :( Cannot create an XMLHttpRequest instance');
        return false;
    }

    url = "https://dog.ceo/api/breeds/list/all"; //url dato dal prof
    httpRequest.onreadystatechange = loadDogs; //funzione sotto
    httpRequest.open('GET', url);
    httpRequest.send();

    //////////////////////////////////////////////////////////////////
    // loadDogs crea la lista che verrà messa nella form di ricerca //
    //////////////////////////////////////////////////////////////////
    function loadDogs()
    {
        if (httpRequest.readyState === XMLHttpRequest.DONE)
        {
            var jsonData = JSON.parse(httpRequest.responseText);

            // if we get a 500 status write the error message parsing it from JSON
            if (httpRequest.status == 500)
            {
                select.innerHTML = "<option value='ServerError'>Server Error</option>";
            }

            else if (httpRequest.status == 200)
            {
                var jsonData = JSON.parse(httpRequest.responseText);
                dogs = jsonData['message'];	//lista dei cani richiesti
                var selectList = "";
                for (var key in dogs)	//per tutti i cani richiesti e trovati
                {
                    //Inserisco nella lista le razze di cani cani negli option value della form
                    selectList += "<option value='" + key + "'>" + key + "</option>";
                }

                select.innerHTML = selectList;	//trasformo in html
            }
        }
        // if we get a 200 status write result table parsing it from JSON



        else
        {
            select.innerHTML = "<option value='ServerError'>Server Error</option>";
        }
    }
    /////////////////////////////
    //FINE FUNZIONE LOADDOGS() //
    /////////////////////////////


})();

/* Store dogs details */
function insertDog() {
    var dogurl = ""; // Url dove inserire i dettagli, probabilmente datoci dal prof

    var dog = [{name : document.getElementById('name').value,
        age: document.getElementById('age').value,
        category: document.getElementById('category').value,
        registrationTime: new Date().toUTCString()
    }];

    $.ajax({
        method: "POST",
        url: dogurl,
        data: JSON.stringify({dog : dog}),
        contentType: 'application/json',
        error: function( status ) {
            alert( "Status: " + status.responseText);
            console.log(status.responseText);
        }
    }).done(function(){
        // Now we reload the page to show the new comment
        //location.replace("page2.html");
    });
}
