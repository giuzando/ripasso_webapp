package it.unipd.dei.webapp.resource;

/**
 * Represents a category of the dog
 */
public class Category {
    private final String name;

    /**
     *
     * @param name Category name
     */
    public Category(String name) {
        this.name = name;
    }

    /**
     *
     * @return Returns the category
     */
    public String getName() {
        return name;
    }
}
