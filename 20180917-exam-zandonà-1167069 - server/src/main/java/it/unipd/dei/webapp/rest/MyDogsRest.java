package it.unipd.dei.webapp.rest;

import it.unipd.dei.webapp.database.*;
import it.unipd.dei.webapp.resource.*;

import java.io.*;
import java.sql.*;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Manages the REST API for Dogs resource
 */
public class MyDogsRest extends RestResource{

    /**
     * Creates a new REST resource for managing Dogs resources.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @param con the connection to the database.
     */
    public MyDogsRest(HttpServletRequest req, HttpServletResponse res, Connection con) {
        super(req, res, con);
    }

    /**
     * Searches dogs by their category.
     *
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void searchDog() throws IOException{

        List<Dogs> lt  = null;
        MessageRest m = null;

        try{
            // parse the URI path to extract the category
            String path = req.getRequestURI();
            path = path.substring(path.lastIndexOf("category") + 8);

            String category = path.substring(1);


            // creates a new object for accessing the database and search the toys
            lt = new SearchDogByCategory(con, category).searchDogByCategory();

            if(lt != null) {
                res.setStatus(HttpServletResponse.SC_OK);
                new ResourceList(lt).toJSON(res.getOutputStream());
            } else {
                // it should not happen
                m = new MessageRest("Cannot search for products: unexpected error while accessing the database.", "E100", null);
                res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                m.toJSON(res.getOutputStream());
            }
        } catch (Throwable t) {
            m = new MessageRest("Cannot search for products: unexpected error while accessing the database.", "E100", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

    }

}
