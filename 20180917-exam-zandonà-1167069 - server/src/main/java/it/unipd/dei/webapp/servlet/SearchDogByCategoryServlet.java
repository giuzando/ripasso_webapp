package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.database.SearchDogByCategoryDatabase;
import it.unipd.dei.webapp.resource.*;
import sun.plugin2.message.Message;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import it.unipd.dei.webapp.resource.MessageRest;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;

public final class SearchDogByCategoryServlet extends AbstractDatabaseServlet {

    /**
     * Searches products by category.
     *
     * @param req
     *            the HTTP request from the client.
     * @param res
     *            the HTTP response from the server.
     *
     * @throws ServletException
     *             if any error occurs while executing the servlet.
     * @throws IOException
     *             if any error occurs in the client/server communication.
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        //request parameter
        String category;

        List<Dogs> el = null;
        MessageRest m = null;

        try {
            category = req.getParameter("category");
            el = new SearchDogByCategoryDatabase(getDataSource().getConnection(), category).searchDogByCategory();
            m = new MessageRest("Dog successfully searched");
        }
        catch (SQLException ex) {
            m = new MessageRest("Cannot search for dogs: unexpected error", "E100", ex.getMessage());
        }

        // set the MIME media type of the response
        res.setContentType("application/json; charset=utf-8");

        // get a stream to write the response
        OutputStream o = res.getOutputStream();

        ResourceList<Dogs> rs = new ResourceList(el);

        rs.toJSON(o);

        o.flush();
        o.close();
    }
}
