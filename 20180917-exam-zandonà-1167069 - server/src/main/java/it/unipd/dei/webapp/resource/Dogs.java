package it.unipd.dei.webapp.resource;

import com.fasterxml.jackson.core.*;

import java.io.*;

/**
 * Represents the data about a toy in REST.
 */
public class Dogs extends Resource{

    /**
     * The id number (identifier) of the dog
     */
    private final int id;

    /**
     * The category of the dog
     */
    private final String category;

    /**
     * Costructor of toy
     *
     * @param id
     *            the id of the dog
     * @param category
     *            the category of the dog
     */
    public Dogs(int id, String category) {
        this.id = id;
        this.category = category;
    }

    /**
     * Return id of dog
     * @return id
     *
     */
    public int getId(){
        return id;
    }

    /**
     * Return category of dog
     * @return category
     *
     */
    public String getCategory(){
        return category;
    }

    //represent dog to JSON
    public void toJSON(OutputStream out) throws IOException {

        final JsonGenerator jg = JSON_FACTORY.createGenerator(out);

        jg.writeStartObject();

        jg.writeFieldName("product");

        jg.writeStartObject();

        jg.writeNumberField("id", id);

        jg.writeStringField("category", category);

        jg.writeEndObject();

        jg.writeEndObject();

        jg.flush();
    }

    /**
     * Creates dog from its JSON representation.
     *
     * @param in the input stream containing the JSON document.
     *
     * @return the toy created from the JSON representation.
     *
     * @throws IOException if something goes wrong while parsing.
     */
    public static Dogs fromJSON(final InputStream in) throws IOException {

        // the fields read from JSON
        int jId = -1;
        String jCategory = null;

        //Obtain a new JsonParser to parse comment from JSON
        final JsonParser jp = JSON_FACTORY.createParser(in);

        // while we are not on the start of an element or the element is not
        // a token element, advance to the next element (if any)
        //Looks for a field called comment which marks the beginning of an dog resource
        while (jp.getCurrentToken() != JsonToken.FIELD_NAME || "product".equals(jp.getCurrentName()) == false) {

            // there are no more events
            if (jp.nextToken() == null) {
                throw new IOException("Unable to parse JSON: no comment object found.");
            }
        }

        //Iterate through the fields of an dog in
        // whatever order until we do not reach the end of the JSON object
        while (jp.nextToken() != JsonToken.END_OBJECT) {

            if (jp.getCurrentToken() == JsonToken.FIELD_NAME) {

                switch (jp.getCurrentName()) {
                    case "id":
                        jp.nextToken();
                        jId = jp.getIntValue();
                        break;
                    case "category":
                        jp.nextToken();
                        jCategory = jp.getText();
                        break;
                }
            }
        }

        //Create and return a new toy from the parsed information
        return new Dogs(jId, jCategory);
    }
}

