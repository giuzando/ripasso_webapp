package it.unipd.dei.webapp.servlet;

import it.unipd.dei.webapp.resource.*;
import it.unipd.dei.webapp.rest.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Manages the REST API for the different REST resources.
 *
 *
 */
public final class RestManagerServlet extends AbstractDatabaseServlet {

    /**
     * The JSON MIME media type
     */
    private static final String JSON_MEDIA_TYPE = "application/json";

    /**
     * The JSON UTF-8 MIME media type
     */
    private static final String JSON_UTF_8_MEDIA_TYPE = "application/json; charset=utf-8";

    /**
     * The any MIME media type
     */
    private static final String ALL_MEDIA_TYPE = "*/*";


    protected final void service(final HttpServletRequest req, final HttpServletResponse res) throws ServletException, IOException {

        res.setContentType(JSON_UTF_8_MEDIA_TYPE);
        final OutputStream out = res.getOutputStream();

        try {

            // if the requested resource was a dog, delegate its processing and return
            if (processDogs(req, res)) {
                return;
            }

            // if none of the above process methods succeeds, it means an unknow resource has been requested
            MessageRest m = new MessageRest("Unknown resource requested.", "E4A6", String.format("Requested resource is %s.", req.getRequestURI()));
            res.setStatus(HttpServletResponse.SC_NOT_FOUND);
            m.toJSON(out);
        } finally {
            // ensure to always flush and close the output stream
            out.flush();
            out.close();
        }
    }


    /**
     * Checks whether the request if for an toys resource and, in case, processes it.
     *
     * @param req the HTTP request.
     * @param res the HTTP response.
     * @return {@code true} if the request was for an toys; {@code false} otherwise.
     *
     * @throws IOException if any error occurs in the client/server communication.
     */
    private boolean processDogs(HttpServletRequest req, HttpServletResponse res) throws IOException {

        final String method = req.getMethod();
        final OutputStream out = res.getOutputStream();

        String path = req.getRequestURI();
        MessageRest m = null;

        // the requested resource was not an toys
        if(path.lastIndexOf("rest/search-product-by-category") <= 0) {
            return false;
        }

        try {
            // the request URI is: /search-product-by-category/category
            if (path.contains("category")) {
                path = path.substring(path.lastIndexOf("category") + 8);

                if (path.length() == 0 || path.equals("/")) {
                    m = new MessageRest("Wrong format for URI: no {category} specified.",
                            "E4A7", String.format("Requesed URI: %s.", req.getRequestURI()));
                    res.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                    m.toJSON(res.getOutputStream());
                } else {
                    switch (method) {
                        case "GET":
                            new MyDogsRest(req, res, getDataSource().getConnection()).searchDog();
                            break;
                        default:
                            m = new MessageRest("Unsupported operation for URI.", "E4A5",
                                    String.format("Requested operation %s.", method));
                            res.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
                            m.toJSON(res.getOutputStream());
                            break;
                    }
                }
            }
        } catch(Throwable t) {
            m = new MessageRest("Cannot search for products: unexpected error while accessing the database.", "E100", t.getMessage());
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            m.toJSON(res.getOutputStream());
        }

        return true;

    }

}
