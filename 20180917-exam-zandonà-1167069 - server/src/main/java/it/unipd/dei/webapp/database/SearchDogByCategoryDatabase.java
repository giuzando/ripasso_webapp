package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Dogs;

import java.sql.*;
import java.util.*;


/**
 * Searches dogs by their category.
 *
 */
public class SearchDogByCategoryDatabase{

    /**
     * The SQL statement to be executed
     */
    private static final String STATEMENT = "SELECT * FROM MyDogs.Product WHERE category = ?"; //è solo un esempio

    /**
     * The connection to the database
     */
    private final Connection con;

    /**
     * The category of the dogs
     */
    private final String category;


    /**
     * Creates a new object for searching toy by category.
     *
     * @param con
     *            the connection to the database.
     * @param category
     *            the category of the toy.
     */
    public SearchDogByCategoryDatabase(Connection con, String category) {
        this.con = con;
        this.category = category;
    }

    /**
     * Searches dog by their category.
     *
     * @return a list of {@code Dogs} object matching the category.
     *
     * @throws SQLException
     *             if any error occurs while searching for toys.
     */
    public List<Dogs> searchDogByCategory() throws SQLException {

        PreparedStatement pstmt = null;
        ResultSet rs = null;

        // the results of the search
        List<Dogs> dogs = new ArrayList<Dogs>();

        try {
            pstmt = con.prepareStatement(STATEMENT);
            pstmt.setString(1, category);

            rs = pstmt.executeQuery();

            while (rs.next()) {
                dogs.add(new Dogs(rs.getInt("id"), rs.getString("category")));
            }
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (pstmt != null) {
                pstmt.close();
            }

            con.close();
        }

        return dogs;
    }

}
