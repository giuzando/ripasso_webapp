var category_list = document.getElementById("searchByCategory");
var image_place = document.getElementById("searcByImage");
var searchBtn = document.getElementById("button_search");
loadCategoryList();

searchBtn.addEventListener("click", function() {
    if(category_list.value==""){
        alert("please insert a category");
    }
    else{
        while (image_place.firstChild){
            image_place.removeChild(image_place.firstChild);
        }
        loadImage();
    }
});


function loadImage(){
    var httpRequest;

    var category = category_list.value;

    var urlCategory = "https://dog.ceo/api/breed/" + category + "/images";

    httpRequest = new XMLHttpRequest();

    if(!httpRequest) {
        alert('Giving up! Cannot create an XMLHttpRequest instance');
        return false;
    }

    httpRequest.onreadystatechange = alertImage;
    httpRequest.open("GET", urlCategory);
    httpRequest.send();


    function alertImage(){
        if(httpRequest.readyState == XMLHttpRequest.DONE){

            var jsonData = JSON.parse(httpRequest.responseText);

            // if we get a 500 status write the error message parsing it from JSON
            if (httpRequest.status == 500)
            {
                category.innerHTML = "<option value='ServerError'>Server Error</option>";
            }

            // if we get a 200 status write result table parsing it from JSON
            else if(httpRequest.status == 200){

                // the JSON success message
                var jsonData = JSON.parse(httpRequest.responseText);
                var resource = jsonData['message'];
                for(var i=1; i<resource.length; i++){

                    var elem = document.createElement("img");
                    elem.setAttribute("src", resource[i]);
                    elem.setAttribute("height","150");
                    elem.setAttribute("alt", resource[i]);
                    elem.style.margin="Bpx";
                    image_place.appendChild(elem);
                }
            }
            // otherwise write a generic error message
            else {
                alert("Ops! There was some problem")
            }
        }
    }
}

// Load dog list. Crea la lista che verrà messa nella form di ricerca
function loadCategoryList(){
    var httpRequest;

    var urlCategoryList = "https://dog.ceo/api/breeds/list/all";

    httpRequest = new XMLHttpRequest();

    if(!httpRequest) {
        alert('Giving up! Cannot create an XMLHttpRequest instance');
        return false;
    }

    httpRequest.onreadystatechange = alertImage;
    httpRequest.open("GET", urlCategoryList);
    httpRequest.send();

    function alertImage(){
        if(httpRequest.readyState === XMLHttpRequest.DONE){

            if(httpRequest.status == 200){

                var jsonData = JSON.parse(httpRequest.responseText);

                //lista dei cani richiesti
                var resource = jsonData['message'];

                //per tutti i cani richiesti e trovati
                for(var i=1; i<resource.length; i++){

                    var elem = document.createElement("img");
                    elem.setAttribute("src", resource[i]);
                    elem.setAttribute("height","150");
                    elem.setAttribute("alt", resource[i]);
                    elem.style.margin="Bpx";
                    image_place.appendChild(elem);
                }
            }
            else {
                alert("Ops! There was some problem")
            }
        }
    }
}